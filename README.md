# PosturePairsDB19

Database of posture pairs recording the change before and after posture training.

The database contains the following fields

***rid*** 
  random user id
 
***position*** 
  text label of the position from which the posture data was recorded from (sitting, standing, hip hinging)

***timestamp*** 
  unix timestamp of the unguided t0 posture snapshot

***posture***
  t0 (unguided) sensor data (five angle offsets in rad)
  
***is_ideal***
  is t0 an unguided (0) or a guided snapshot (1) ?

***timestamp2***
  unix timestamp of the guided t1 posture snapshot

***posture2***
  t1 (guided) sensor data (five angle offsets in rad)

The posture data (in posture and posture2) are saved as angular offsets in radians (relative sensor angles). The data can be converted to absolute sensor angles by computing its cumulative sum.

**This dataset is the supplementary material of the open access publication:** 
[SMHK19] Katharina Stollenwerk, Jonas Müller, André Hinkenjann and Björn Krüger, *Analyzing Spinal Shape Changes During Posture Training Using a Wearable Device*, Sensors 2019, 19(16), 3625; **[https://doi.org/10.3390/s19163625](https://doi.org/10.3390/s19163625).**

If you're using this database in a publication please cite the above article.
